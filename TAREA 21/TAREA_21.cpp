struct automovil
{
	char marca[20];
	char modelo[20];
	int a�o;
	char color[15];
}

struct computadora
{
	char marca[20];
	char modelo[20];
	char procesador[20];
	int capacidadDiscoDuro;
	int capacidadRAM;
}

struct alumno
{
	char nombre[30];
	char sexo[2];'M','F';
	char materia[20];
	int edad;
	int numeroCuenta;
	char plantel[20];
}

struct equipoDeFutbol
{
	char nombre[30];
	int numeroDeJugadores;
	char nombreDeDirectorTecnico[20];
	char estadio[25];
}

struct mascota
{
	char raza[20];
	char nombre[20];
	char color[20];
	char sexo[2];'M','H';
	char talla[3];'CH','M','G';
}

struct videojuego
{
	char nombre[25];
	char categoria[30];
	char especificacion[25];
	char plataforma[20];
}

struct superHeroe
{
	char nombre[20];
	char poder[20];
	char enemigo[20];
}

struct planetas
{
	char nombre[20];
	char color[20];
	int numSatelites;
	int dimenciones;
}

struct personajeRainbowSixSiege
{
	char nombre[20];
	char habilidad[20];
	char arma[20];
	char nacionalidad[20];
	int edad;
}

struct comic
{
	char nombre[30];
	char autor[25];
	int numDePaginas;
	int numSerie;
	
}
